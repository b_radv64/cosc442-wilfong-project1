package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JPanel;

import edu.towson.cis.cosc442.project1.monopoly.MonopolyGUI;

public class North extends South {

	protected JPanel northPanel = new JPanel();

	public North() throws HeadlessException {
		super();
	}

	public North(GraphicsConfiguration arg0) {
		super(arg0);
	}

	public North(String arg0) throws HeadlessException {
		super(arg0);
	}

	public North(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
	}

}