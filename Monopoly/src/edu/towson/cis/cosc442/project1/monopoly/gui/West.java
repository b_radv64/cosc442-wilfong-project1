package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import edu.towson.cis.cosc442.project1.monopoly.MonopolyGUI;

public class West extends JFrame {

	protected JPanel westPanel = new JPanel();

	public West() throws HeadlessException {
		super();
	}

	public West(GraphicsConfiguration arg0) {
		super(arg0);
	}

	public West(String arg0) throws HeadlessException {
		super(arg0);
	}

	public West(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
	}

}