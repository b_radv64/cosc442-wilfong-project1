package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JPanel;

import edu.towson.cis.cosc442.project1.monopoly.MonopolyGUI;

public class East extends North {

	protected JPanel eastPanel = new JPanel();

	public East() throws HeadlessException {
		super();
	}

	public East(GraphicsConfiguration arg0) {
		super(arg0);
	}

	public East(String arg0) throws HeadlessException {
		super(arg0);
	}

	public East(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
	}

}