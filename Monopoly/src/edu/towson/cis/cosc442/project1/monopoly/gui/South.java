package edu.towson.cis.cosc442.project1.monopoly.gui;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JPanel;

import edu.towson.cis.cosc442.project1.monopoly.MonopolyGUI;

public class South extends West {

	protected JPanel southPanel = new JPanel();

	public South() throws HeadlessException {
		super();
	}

	public South(GraphicsConfiguration arg0) {
		super(arg0);
	}

	public South(String arg0) throws HeadlessException {
		super(arg0);
	}

	public South(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
	}

}